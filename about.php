<?php
include_once('./controllers/ImageController.php');
include_once('./controllers/DescController.php');
$imageController = new ImageController;
$descController = new DescController;
$location = "about";
$images = $imageController->getAllByLocation($location);
$descs = $descController->getAllByLocation($location);
$location = "about_team";
$team = $descController->getAllByLocation($location);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!--<link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/bebasneueregular" type="text/css" />-->
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <!--<link rel="stylesheet" media="screen" href="https://fonts.google.com/specimen/Open+Sans?selection.family=Open+Sans" type="text/css" />-->
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <title>Gaming Portal</title>
</head>

<body>
    <?php include 'header.php' ?>
    <main id="about">

        <div class="container">
            <div class="about_content">
                <div class="about_content__logo">
                    <img src=<?php echo $images[0]['image_location']; ?>>
                </div>
                <div class="about_content">
                    <div class="about_content__title">
                        <h1>About us</h1>
                    </div>

                    <div class="about_content__desc">
                        <p>
                            <?php echo $descs[0]['text_desc'] ?>
                        </p>
                        <br />
                        <div class="team">
                            <h1>Team members</h1>
                            <div class="team_members">
                                <?php foreach ($team as $member) : ?>
                                    <h3> <?php echo $member['text_desc'] ?></h3>
                                <?php endforeach ?>
                            </div>
                            <div class="team_image">
                                <img src=<?php echo $images[1]['image_location']; ?>>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include 'footer.php' ?>
</body>

</html>