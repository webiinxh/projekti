$(document).ready(function() {
    $('.main_banner__slider').owlCarousel({
        loop: true,
        nav: true,
        margin: 10,
        dots: false,
        items: 1,
        autoplay: true,
        autoplayHoverPause: true,
        animateOut: 'fadeOut',

    })
});