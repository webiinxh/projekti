    function send() {
        var emri = document.getElementById('emri_input');
        var mbiemri = document.getElementById('mbiemri_input');
        var email = document.getElementById('email_input');
        var gjinia = document.querySelector('input[name="gender"]:checked');
        var qytetet = document.getElementById('lista_qyteteve');
        var qyteti = qytetet.options[qytetet.selectedIndex];
        var mesazhi = document.getElementById('mesazhi');
        var data = document.getElementById('birth_date');
        var isString = "[a-zA-Z]{2,20}$";
        var isEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var valid = true;
        var text = "Ju lutem plotesoni kutite me ngjyre te kuqe.";

        if (!emri.value.match(isString)) {
            emri.style.borderColor = "red";
            valid = false;
        } else
            emri.style.borderColor = "grey";
        if (!mbiemri.value.match(isString)) {
            mbiemri.style.borderColor = "red";
            valid = false;
        } else
            mbiemri.style.borderColor = "grey";
        if (!email.value.match(isEmail)) {
            email.style.borderColor = "red";
            valid = false;
        } else
            email.style.borderColor = "grey";

        if (qyteti.value == 1) {
            qytetet.style.borderColor = "red";
            valid = false;
        } else
            qytetet.style.borderColor = "grey";
        if (data.value == '') {
            data.style.borderColor = "red";
            valid = false;
        } else
            data.style.borderColor = "grey";
        if (mesazhi.value == '' || mesazhi.value.length < 5) {
            mesazhi.style.borderColor = "red";
            valid = false;
        } else
            data.style.borderColor = "grey";

        if (valid == true) {
            console.log(emri.value);
            console.log(mbiemri.value);
            console.log(email.value);
            if (gjinia != null)
                console.log(gjinia.value);
            else
                console.log("Gjinia nuk eshte obligative");
            console.log(qyteti.value);
            console.log(data.value);
            var parent = document.querySelector('.contact_content__title > h1');
            var child = document.querySelector('.contact_content__title > h1 > p');
            if (child != null) {
                parent.removeChild(child);

            }
            return true;

        } else {

            var title = document.querySelector('.contact_content__title > h1');

            if (!title.textContent.includes(text)) {
                var warning = document.createElement("p");
                var warningText = document.createTextNode(text);

                warning.appendChild(warningText);
                warning.style.fontSize = "20px";
                warning.style.color = "red";
                warning.style.letterSpacing = "1px";
                title.appendChild(warning);
                return false;

            }

        }


    }