<?php
class Database
{
    public $pdo;

    public function __construct()
    {
        try {
            if (!isset($_SESSION))
                session_start();
            $this->pdo = new PDO('mysql:host=127.0.0.1;dbname=gamingportal', 'root', '');
        } catch (PDOException $e) {
            die('DIE' . $e->getMessage());
        }
    }
}
