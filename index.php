<?php
require_once('./controllers/ImageController.php');
require_once('./controllers/DescController.php');
$imageController = new ImageController;
$descController = new DescController;
$location = "index";
$articleImgs = $imageController->getAllByLocation($location);
$location = "index_article";
$descs = $descController->getAllByLocation($location);
$location = "index_cat";
$categories = $descController->getAllByLocation($location);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" type="text/css" media="screen">
    <!--<link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/bebasneueregular" type="text/css" />-->
    <!--<link rel="stylesheet" media="screen" href="https://fonts.google.com/specimen/Open+Sans?selection.family=Open+Sans" type="text/css" />-->
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/vendor.css" rel="stylesheet" type="text/css">

    <title>Gaming Portal</title>
</head>

<body>
    <?php include 'header.php' ?>
    <main id="main">
        <?php
        $location = "index";
        include 'slider.php';
        ?>

        <div class="inline_articles">
            <div class="container">
                <h2>Latest news</h2>
                <div class="row">

                    <div class="inline_articles__container">
                        <?php for ($i = 0; $i < 3; $i++) : ?>
                            <div class="inline_articles__item">
                                <div class="inline_articles__thumb">
                                    <a href="#">
                                        <img src=<?php echo $articleImgs[0]['image_location']; ?>>
                                    </a>
                                </div>
                                <div class="inline_articles__text">
                                    <a href="#">
                                        <h4 class="inline_articles__title"><?php echo $descs[0]['text_desc']; ?>
                                        </h4>
                                        <span class="inline_articles__divider"></span>
                                        <span class="inline_articles__cat"><?php echo $categories[0]['text_desc']; ?></span>
                                    </a>
                                </div>
                            </div>
                        <?php endfor ?>
                    </div>

                </div>
            </div>
        </div>
        </div>
        <div class="inline_articles inline_articles--four_items">
            <div class="container">
                <h2>Other news</h2>
                <div class="row">
                    <div class="inline_articles__container">
                        <?php for ($i = 0; $i < 8; $i++) : ?>
                            <div class="inline_articles__item">
                                <div class="inline_articles__thumb">
                                    <a href="#">
                                        <img src=<?php echo $articleImgs[0]['image_location']; ?>>
                                    </a>
                                </div>
                                <div class="inline_articles__text">
                                    <a href="#">
                                        <h4 class="inline_articles__title"><?php echo $descs[0]['text_desc']; ?>
                                        </h4>
                                        <span class="inline_articles__divider"></span>
                                        <span class="inline_articles__cat"><?php echo $categories[0]['text_desc']; ?></span>
                                    </a>
                                </div>
                            </div>
                        <?php endfor ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include 'footer.php' ?>

</body>
<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
<script src="js/owl.carousel.min.js">
</script>
<script src="js/main.js"></script>

</html>