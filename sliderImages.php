<?php

require_once('./controllers/SliderController.php');

$slider = new SliderController;

if (isset($_POST['inserted'])) {
    $_POST['image_location'] = $_FILES['image_location'];
    $slider->store($_POST);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!--<link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/bebasneueregular" type="text/css" />-->
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <!--<link rel="stylesheet" media="screen" href="https://fonts.google.com/specimen/Open+Sans?selection.family=Open+Sans" type="text/css" />-->
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/vendor.css" rel="stylesheet" type="text/css">

    <title>Gaming Portal</title>
</head>

<?php include 'header.php' ?>

<body>
    <?php if (isset($_SESSION['is_admin']) && $_SESSION['is_admin']) : ?>

        <div class="container">
            <?php
            $location = "sliderImages";
            include 'slider.php'; ?>

            <form method="post" enctype="multipart/form-data">
                <div class="input_holder">
                    <label class="form_label" for="image_location">Image:</label>
                    <input class="form_box" type="file" name="image_location" id="image_location" />
                </div>
                <div class="input_holder">
                    <label class="form_label" for="image_name">Name:</label>
                    <input class="form_box" type="text" name="image_name" id="image_name" />
                </div>
                <div class="button_holder">
                    <input class="contact_button" style="margin: 0 auto" type="submit" name="inserted" id="insert" value="Insert" />
                </div>
            </form>
        </div>
    <?php else : ?>
        <h1>Access Denied.</h1>
    <?php endif; ?>
    <?php include 'footer.php' ?>
</body>

<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
<script src="js/owl.carousel.min.js">
</script>
<script src="js/main.js"></script>


</html>