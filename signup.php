<?php
require './controllers/UserController.php';

$user = new UserController;

if (isset($_POST['submitted'])) {
    $user->store($_POST);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
        <!--<link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/bebasneueregular" type="text/css" />-->
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <title>Gaming Portal</title>
</head>

<body>
    <?php include 'header.php' ?>
    <content class="signup">
        <img src="images\login-background.jpg">
        <div class="loginbox">

            <h1>Sign up Here</h1>
            <form method="post">
                <p>Username</p>
                <input type="text" name="username" placeholder="Username">
                <p>Email</p>
                <input type="text" name="email" placeholder="Email">
                <p>Password</p>
                <input type="password" name="password_1" placeholder="Password">
                <p>Confirm Password</p>
                <input type="password" name="password_2" placeholder="Confirm Password">
                <?php if (isset($_SESSION['is_admin']) && $_SESSION['is_admin']) : ?>
                    <input type="checkbox" name="is_admin">Is Admin?
                <?php endif ?>
                <input type="submit" name="submitted" value="signup">

            </form>
        </div>

    </content>


    <?php include 'footer.php' ?>
</body>

</html>