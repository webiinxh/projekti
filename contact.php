<?php 
require './controllers/EmailController.php';
$emailController = new EmailController;
if(isset($_POST['sent'])){
$emailController->send($_POST);
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!--<link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/bebasneueregular" type="text/css" />-->
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <!--<link rel="stylesheet" media="screen" href="https://fonts.google.com/specimen/Open+Sans?selection.family=Open+Sans" type="text/css" />-->
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <title>Gaming Portal</title>
</head>

<body>
    <?php include 'header.php' ?>
    <main id="contact">
        <div class="container">
            <div class="contact_content">
                <div class="contact_content__title">
                    <h1>Kontakti</h1>
                </div>
                <div class="contact_content__form">
                    <form method = "post">
                        <div class="input_holder">
                            <label for="emri_input" class="form_label">First Name*</label>
                            <input class="form_box" id="emri_input" type="text" name="emri" placeholder="Enter First Name">
                        </div>

                        <div class="input_holder">
                            <label for="mbiemri_input" class="form_label">Last Name*</label>
                            <input class="form_box" id="mbiemri_input" type="text" name="mbiemri"placeholder="Enter Surname">
                        </div>

                        <div class="input_holder">
                            <label for="email_input" class="form_label">Email*</label>
                            <input class="form_box" id="email_input" type="text" name="email" placeholder="Email">
                        </div>

                        <h3>Gender</h3>
                        <div class="input_holder">
                            <input type="radio" id="male" name="gender" value="M">
                            <label for="male" class="form_label">Male</label>
                            <input type="radio" id="female" name="gender" value="F">
                            <label for="female" class="form_label">Female</label>
                        </div>

                        <div class="input_holder">
                            <label for="lista_qyteteve" class="form_label">City*</label>
                            <select id="lista_qyteteve" class="form_box">
                                <option value="1">City</option>
                                <option value="2">Prishtine</option>
                                <option value="3">Prizren</option>
                                <option value="4">Peja</option>
                                <option value="5">Gjakova</option>
                                <option value="6">Ferizaj</option>
                                <option value="7">Besiana</option>
                                <option value="8">Mitrovica</option>
                                <option value="9">Tjeter/Other</option>
                            </select>
                        </div>
                        <div class="input_holder">
                            <label for="birth_date" class="form_label">Date of Birth*</label>
                            <input type="date" name ="birth_date" id="birth_date" class="form_box" value="dd/mm/yyyy" min="1930-01-01" max="2012-12-31">
                        </div>
                        <div class="input_holder">
                            <label for="mesazhi" class="form_label">Your message:</label>
                            <textarea id="mesazhi" name = "mesazhi" class="form_box" placeholder="Type your message here..."></textarea>
                            <input class="contact_button" name ="sent" type="submit" onclick="return send();" value="Send">
                        </div>
                    </form>
                    <div class="contact_content__kontakti">
                        <h4>Për cdo pyetje që keni rreth portalit mund të na kontaktoni në:</h4>
                        <br />
                        <p>Tel: 044-333-333<br />
                            Fax: 038-333-333<br />
                            Email: gaming_info@ubt-uni.com</p>
                        <div class="contact_content__map">
                            <div class="contact_content__address">
                                <h4>Neve mund të na gjeni edhe në lokacionin e mëposhtëm:</h4>
                                <br />
                                <p>Address: Lagjja Kalabria, 10000, Prishtinë</p>
                            </div>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2045.3918960967173!2d21.15216743828902!3d42.64662419345485!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa31dd05b21bd09de!2sUniversiteti+p%C3%ABr+Biznes+dhe+Teknologji!5e0!3m2!1sen!2s!4v1558827867831!5m2!1sen!2s" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="contact_content__info">



                </div>
                <div id="social">
                    <a href="https://www.facebook.com" target="_blank"><img src="images/fb-logo.png" alt="facebook" height="40px" width="40px"></a>
                    <a href="https://www.instagram.com" target="_blank"><img src="images/instagram-logo.png" alt="Instagram" height="40px" width="40px"></a>
                    <a href="https://www.twitter.com" target="_blank"><img src="images/twitter-logo.png" alt="Twitter" height="40px" width="40px"></a>
                    <a href="https://www.linked.com" target="_blank"><img src="images/in-logo.png" alt="Linked" height="40px" width="40px"></a>
                    <a href="https://www.youtube.com/" target="_blank"><img src="images/youtube-logo.png" alt="Youtube" height="40px" width="40px"></a>
                </div>

            </div>


        </div>
    </main>



    <?php include 'footer.php' ?>
</body>
<script src="js/validation.js">
</script>

</html>