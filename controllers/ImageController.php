<?php

require_once('./core/database.php');

class ImageController
{
    protected $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAll()
    {
        $query = $this->db->pdo->query('SELECT * FROM gallery');
        return $query->fetchAll();
    }
    public function getAllByLocation($location)
    {
        $query = $this->db->pdo->query("SELECT * FROM gallery WHERE image_name LIKE '$location%'");
        return $query->fetchAll();
    }

    public function store($request)
    {
        $query = $this->db->pdo->prepare('INSERT INTO gallery (image_file) VALUES (:image_file)');
        $query->bindParam(':image_file', $request['image_file'], PDO::PARAM_LOB);
        $query->execute();
        header('Location: index.php');
    }
}
