<?php
include './core/database.php';

class UserController
{
    protected $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAll()
    {
        $query = $this->db->pdo->query('SELECT * FROM users');
        return $query->fetchAll();
    }

    public function store($request)
    {
        echo $request['is_admin'];
        if ($request['is_admin'] == null)
            $is_admin = false;
        else
            $is_admin = $request['is_admin'];
        $password = password_hash($request['password_1'], PASSWORD_DEFAULT);
        $query = $this->db->pdo->prepare('INSERT INTO users (username,email,password,is_admin) VALUES (:username,:email,:password,:is_admin)');
        $query->bindParam(':username', $request['username'], PDO::PARAM_STR);
        $query->bindParam(':email', $request['email'], PDO::PARAM_STR);
        $query->bindParam(':password', $password, PDO::PARAM_STR);
        $query->bindParam(':is_admin', $is_admin, PDO::PARAM_BOOL);
        $query->execute();
        header('Location: login.php');
    }
}
