<?php

require_once('./core/database.php');

class ShopController
{
    protected $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAll()
    {
        $query = $this->db->pdo->query("SELECT * FROM tbl_product");
        return $query->fetchAll();
    }

    public function store($request)
    {
        $query = $this->db->pdo->prepare('INSERT INTO orders (user_id,product_id,quantity) VALUES (:user_id,:product_id,:quantity)');
        $query->bindParam(':product_id', $request['product_id'], PDO::PARAM_INT);
        $query->bindParam(':user_id', $request['user_id'], PDO::PARAM_INT);
        $query->bindParam(':quantity', $request['quantity'], PDO::PARAM_INT);
        $query->execute();
        header('Location: buy.php');
    }
    public function deleteAll($request){

        $query = $this->db->pdo->prepare('DELETE FROM orders WHERE user_id = :user_id && product_id = :product_id');
        $query->bindParam(':product_id', $request['product_id'], PDO::PARAM_INT);
        $query->bindParam(':user_id', $request['user_id'], PDO::PARAM_INT);
        $query->execute();
    }
}
