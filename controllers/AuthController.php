<?php
require_once('./core/database.php');

class AuthController
{
    protected $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function login($request)
    {
        $query = $this->db->pdo->prepare('SELECT id,username,email,password,is_admin FROM users WHERE username = :username');
        $query->bindParam(':username', $request['username'], PDO::PARAM_STR);
        $query->execute();

        $user = $query->fetch();
        //$password = password_hash($request['password'], PASSWORD_DEFAULT);
        if ($user != null && password_verify($request['password'], $user['password'])) {
            session_start();

            $_SESSION['user_id'] = $user['id'];
            $_SESSION['username'] = $user['username'];
            $_SESSION['email'] = $user['email'];
            $_SESSION['is_admin'] = $user['is_admin'];
            header("Location: index.php");
        } else {
            echo "<script> alert('Bad Credentials')</script>";
        }
    }
}
