<?php
require_once('./core/database.php');

class SliderController
{
    protected $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAll()
    {
        $query = $this->db->pdo->query('SELECT * FROM slider');
        return $query->fetchAll();
    }

    public function store($request)
    {
        $image_location = "./images/" . $request['image_location']['name'];
        move_uploaded_file($request['image_location']['tmp_name'], $image_location);
        $query = $this->db->pdo->prepare('INSERT INTO slider (image_name,image_location) VALUES (:image_name,:image_location)');
        $query->bindParam(':image_location', $image_location, PDO::PARAM_STR);
        $query->bindParam(':image_name', $request['image_name'], PDO::PARAM_STR);
        $query->execute();
    }
    public function remove($request)
    {
        unlink($request['hidden_location']);
        $query = $this->db->pdo->prepare('DELETE FROM Slider WHERE image_location = :image_location');
        $query->bindParam(':image_location', $request['hidden_location'], PDO::PARAM_STR);
        $query->execute();
    }
}
