<?php
require './controllers/UserController.php';

$user = new UserController;
$users = $user->getAll();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!--<link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/bebasneueregular" type="text/css" />-->
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <!--<link rel="stylesheet" media="screen" href="https://fonts.google.com/specimen/Open+Sans?selection.family=Open+Sans" type="text/css" />-->
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/vendor.css" rel="stylesheet" type="text/css">

    <title>Gaming Portal</title>
    <style> 
    table{
        border-collapse: collapse;
        width  :100%;
        color: #303030;
        font-size: 25px;
        text-align: left;
    }
    th{
        background-color: #303030;
        color: white;
    }
    tr:nth-child(even){background-color:#f2f2f2}
</style>    
    
</head>

<body>
    <?php include 'header.php' ?>
    <div class="container">
        <?php if (isset($_SESSION['is_admin']) && $_SESSION['is_admin']) : ?>
            <a href="signup.php"style="font-size:28px; float: right;background:white" onmouseover="this.style.color='red';" onmouseout="this.style.color='black';">Create User</a>
            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Admin Permissions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user) : ?>
                        <tr>
                            <td> <?php echo $user['id'] ?></td>
                            <td> <?php echo $user['username'] ?></td>
                            <td> <?php echo $user['email'] ?></td>
                            <td> <?php echo $user['is_admin'] ?></td>

                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else : ?>
            <h1>Access Denied.</h1>
        <?php endif; ?>
    </div>
    <?php include 'footer.php' ?>
</body>

</html>