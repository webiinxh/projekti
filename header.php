<?php
if (!isset($_SESSION))
    session_start();
?>
<header id="header" class="main_header">
    <div class="container">
        <a class="main_header__logo" href="index.php">
            <div class="main_header__image">
                <img src="images/logo1.jpg">
            </div>
            <div class="main_header__title">
                <h2>Gaming Portal</h2>
            </div>
        </a>
        <div class="main_header__navigation">
            <ul>
                <li>
                    <a href="index.php">
                        Home
                    </a>
                </li>
                <li>
                    <a href="buy.php">
                        <img src="images\shop.png" width="30px" height="18px" style="margin-right: 3px;">Shop
                    </a>
                </li>
                <li>
                    <a href="about.php">
                        About us
                    </a>
                </li>
                <li>
                    <a href="contact.php">
                        Contact
                    </a>
                </li>
                <?php if (isset($_SESSION['username'])) : ?>
                    <li>
                        <a href="#">Hi, <?php echo $_SESSION['username'] ?></a>
                        <ul class="nested_list">
                            <li>
                                <?php if ($_SESSION['is_admin'] == true) : ?>
                                    <a href="sliderImages.php">Slider</a>
                                    <a href="users.php">Users</a>
                                    <?php endif; ?>
                                <a href="logout.php">Logout</a>

                            </li>
                        </ul>

                    </li>
                <?php else : ?>
                    <li>
                        <a href="login.php">Login</a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</header>