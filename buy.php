<?php
require_once("./controllers/ShopController.php");

$shopController = new ShopController;

$articlesToSell = $shopController->getAll();

if (isset($_POST["add_to_cart"])) {
	$_POST['user_id'] = $_SESSION['user_id'];
	$_POST['product_id'] = $_GET['id'];
	$_POST['quantity'] = $_POST['quantity'];
	$shopController->store($_POST);

	if (isset($_SESSION["shopping_cart"])) {

		$item_array_id = array_column($_SESSION["shopping_cart"], "item_id");
		if (!in_array($_GET["id"], $item_array_id)) {
			$count = count($_SESSION["shopping_cart"]);
			$item_array = array(
				'item_id'			=>	$_GET["id"],
				'item_name'			=>	$_POST["hidden_name"],
				'item_price'		=>	$_POST["hidden_price"],
				'item_quantity'		=>	$_POST["quantity"]
			);
			$_SESSION["shopping_cart"][$count] = $item_array;
		} else {
			$count = count($_SESSION["shopping_cart"]) - 1;
			$_SESSION['shopping_cart'][$count]['item_quantity'] += $_POST["quantity"];
			$vartest = $_SESSION['shopping_cart'][$count]['item_quantity'];
			echo "<script>console.log($vartest)</script>";
		}
	} else {
		$item_array = array(
			'item_id'			=>	$_GET["id"],
			'item_name'			=>	$_POST["hidden_name"],
			'item_price'		=>	$_POST["hidden_price"],
			'item_quantity'		=>	$_POST["quantity"]
		);
		$_SESSION["shopping_cart"][0] = $item_array;
	}
}

if (isset($_GET["action"])) {
	if ($_GET["action"] == "delete") {
		foreach ($_SESSION["shopping_cart"] as $keys => $values) {
			if ($values["item_id"] == $_GET["id"]) {
				unset($_SESSION["shopping_cart"][$keys]);
				$_POST['user_id'] = $_SESSION['user_id'];
				$_POST['product_id'] = $_GET['id'];
				$shopController->deleteAll($_POST);
				echo '<script>alert("Item Removed")</script>';
				echo '<script>window.location="buy.php"</script>';
			}
		}
	}
}

?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<!--<link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/bebasneueregular" type="text/css" />-->
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
	<!--<link rel="stylesheet" media="screen" href="https://fonts.google.com/specimen/Open+Sans?selection.family=Open+Sans" type="text/css" />-->
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/vendor.css" rel="stylesheet" type="text/css">
	<title>Shop Game</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />-->
	<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
	<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
	<script src="js/owl.carousel.min.js">
	</script>
	<script src="js/main.js"></script>

</head>
<?php include("header.php"); ?>

<body>
	<div class="container">
		<h3 align="center" style="margin:5px 0">Shopping Cart</h3>
		<?php
		if (count($articlesToSell) > 0) {
			?>
			<div class="shop_content">
				<?php foreach ($articlesToSell as $row) {
					?>
					<div class="shop_item">
						<form method="post" action="buy.php?action=add&id=<?php echo $row["id"]; ?>">

							<img src="images/<?php echo $row["image"]; ?>" class="shop_item__image" />

							<h4 class="text-info"><?php echo $row["name"]; ?></h4>

							<h4 class="text-danger">$ <?php echo $row["price"]; ?></h4>

							<input type="text" name="quantity" value="1" class="form-control" />

							<input type="hidden" name="hidden_name" value="<?php echo $row["name"]; ?>" />

							<input type="hidden" name="hidden_price" value="<?php echo $row["price"]; ?>" />
							<?php if (isset($_SESSION['username'])) : ?>
								<input type="submit" name="add_to_cart" class="btn btn-success" value="Add to Cart" />

							<?php else : ?>
								<a href="login.php" class="btn btn-success">Add to Cart</a>
							<?php endif ?>
					</div>
					</form>
				<?php
				}
				?>
			</div>
		<?php }
		?>
		<div style="clear:both"></div>
		<h3 style="margin:5px 0">Order Details</h3>
		<div class="table-responsive">
			<table class="table table-bordered">
				<tr>
					<th width="40%">Item Name</th>
					<th width="10%">Quantity</th>
					<th width="20%">Price</th>
					<th width="15%">Total</th>
					<th width="5%">Action</th>
				</tr>
				<?php
				if (!empty($_SESSION["shopping_cart"])) {
					$total = 0;
					foreach ($_SESSION["shopping_cart"] as $keys => $values) {
						?>
						<tr>
							<td><?php echo $values["item_name"]; ?></td>
							<td><?php echo $values["item_quantity"]; ?></td>
							<td>$ <?php echo $values["item_price"]; ?></td>
							<td>$ <?php echo number_format($values["item_quantity"] * $values["item_price"], 2); ?></td>
							<td><a style="color: #337ab7;" href="buy.php?action=delete&id=<?php echo $values["item_id"]; ?>"><span class="text-danger">Remove</span></a></td>
						</tr>
						<?php
						$total = $total + ($values["item_quantity"] * $values["item_price"]);
					}
					?>
					<tr>
						<td colspan="3" align="right">Total</td>
						<td align="right">$ <?php echo number_format($total, 2); ?></td>
						<td></td>
					</tr>
				<?php
				}
				?>

			</table>
		</div>
	</div>
	<?php include 'footer.php' ?>
	</div>

</body>

</html>