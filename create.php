<?php

require './controllers/UserController.php';

$user = new UserController;

if (isset($_POST['submitted'])) {
    $user->store($_POST);
}

?>

<!DOCTYPE html>

<html lang="eng">

<head>
    <meta charset="UTF-8">
    <title>Create</title>
</head>

<body>
    <form action="" method="POST">
        <input type="text" name="emri">
        <input type="text" name="mbiemri">
        <input type="text" name="email">
        <input type="password" name="password">
        <button type="submit" name="submitted">Create</button>
    </form>
</body>

</html>