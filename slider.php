<?php
require_once('./controllers/SliderController.php');

$sliderController = new SliderController;
$images = $sliderController->getAll();

if (isset($_POST['removed'])) {
    $sliderController->remove($_POST);
}
?>

<div class="main_banner">
    <div class="container">

        <div class="main_banner__slider owl-carousel">
            <?php foreach ($images as $image) : ?>
                <div class="main_banner__item">
                    <div class="main_banner__thumb">
                        <a href="#" class="next round">
                        </a>
                        <img src=<?php echo $image['image_location'] ?>>
                    </div>
                    <div class="main_banner__text">
                        <?php if ($location == "index") : ?>
                            <a href="#" class="cat_button">Read More</a>
                        <?php else : ?>
                            <form method="post">
                                <input type="submit" name="removed" class="remove_button" value="Remove">
                                <input type="hidden" name="hidden_name" value=<?php echo $image['image_name']; ?> />
                                <input type="hidden" name="hidden_location" value=<?php echo $image['image_location']; ?> />
                            </form>
                        <?php endif ?>
                        <h4 class="main_banner__title"><?php echo $image['image_name'] ?></h4>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</div>