<?php
require './controllers/AuthController.php';

$auth = new AuthController;

if (isset($_POST['submitted'])) {

    $auth->login($_POST);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!--<link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/bebasneueregular" type="text/css" />-->
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <title>Gaming Portal</title>
</head>

<body>
    <?php include 'header.php' ?>
    <content class="login">
        <img src="images\login-background.jpg">
        <div class="loginbox">
            <img src="images\avatar.png" class="avatar">
            <h1>Login Here</h1>
            <form method="post">
                <p>Username</p>
                <input type="text" name="username" placeholder="Username">
                <p>Password</p>
                <input type="password" name="password" placeholder="Password">
                <input type="submit" name="submitted" value="Login">
                <a href="signup.php"> Don't have account?</a>
            </form>
        </div>

    </content>


    <?php include 'footer.php' ?>
</body>

</html>